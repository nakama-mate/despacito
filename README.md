Despacito
===

Joint project.

## Installation

- Use Python 3.7 or ... I don't know.
- Use Python framework build. wxPython doesn't work without framework build.
- Install libraries from requirements.txt.

```bash
$ cd src/
$ python main.py
```

## Build Standalone Executable File

- Install **pyinstaller** package from requirement.txt.
- Use the below command to build and place built files.
	- noconsole: when exe file is executed, the console window will not be shown.
	- distpath: Where to put the bundled app (default: ./dist)
	- workpath: Where to put all the temporary work files, .log, .pyz and etc. (default: ./build)

```bash
$ cd src/
$ pyinstaller -F --noconsole main.py --distpath /your/path --workpath /your/work/path
```