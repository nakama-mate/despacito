import wx
import despacito_searcher
import util
import os


# wx.Frame
# https://wxpython.org/Phoenix/docs/html/wx.Frame.html
class DespacitoFrame(wx.Frame):

    def __init__(self):

        super(DespacitoFrame, self).__init__(
            None,  # parent
            id=wx.ID_ANY,
            title='Despacito',
            pos=wx.DefaultPosition,
            style=wx.CAPTION | wx.STAY_ON_TOP,
        )

        # Switch False when our Window is closed manually.
        self.window_is_alive = True

        # Create Frame's appearance. Such as Panel, TextCtrl and ListBox.
        self.CreateFrameAppearance()

        # Register events to the Frame.
        self.RegisterFrameEvents()


    def CreateFrameAppearance(self):
        """Create Frame's appearance. The structure is like below.

        - Frame
            - Panel
                - Vertical BoxSizer
                    - Horizontal BoxSizer
                        - TextCtrl
                    - ListBox
        """

        # wx.Panel
        # https://wxpython.org/Phoenix/docs/html/wx.Panel.html#wx.Panel
        panel = wx.Panel(
            self,  # parent
            id=wx.ID_ANY,
            pos=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=wx.TAB_TRAVERSAL,
        )

        # wx.BoxSizer
        # https://wxpython.org/Phoenix/docs/html/wx.BoxSizer.html#wx.BoxSizer
        box_sizer_vertical = wx.BoxSizer(orient=wx.VERTICAL)
        box_sizer_horizontal = wx.BoxSizer(orient=wx.HORIZONTAL)
        # wx.Window.SetSizerAndFit
        # https://wxpython.org/Phoenix/docs/html/wx.Window.html#wx.Window.SetSizerAndFit
        panel.SetSizerAndFit(box_sizer_vertical, deleteOld=True)

        # wx.TextCtrl
        # https://wxpython.org/Phoenix/docs/html/wx.TextCtrl.html#wx.TextCtrl
        self.text_ctrl = wx.TextCtrl(
            panel,
            id=wx.ID_ANY,
            value='',
            pos=wx.DefaultPosition,
            size=wx.DefaultSize,
            style=wx.TE_LEFT,
            validator=wx.DefaultValidator,
        )
        self.text_ctrl.Bind(wx.EVT_TEXT, self.OnText)
        box_sizer_horizontal.Add(self.text_ctrl, proportion=1)

        # wx.ListBox
        # https://wxpython.org/Phoenix/docs/html/wx.ListBox.html#wx.ListBox
        self.list_box = wx.ListBox(
            panel,
            id=wx.ID_ANY,
            pos=wx.DefaultPosition,
            size=wx.DefaultSize,
            choices=[],
            style=wx.LB_SINGLE | wx.LB_NEEDED_SB,
        )

        # wx.Sizer.Add
        # https://wxpython.org/Phoenix/docs/html/wx.Sizer.html#wx.Sizer.Add
        box_sizer_vertical.Add(box_sizer_horizontal,
                               proportion=0, flag=wx.EXPAND)
        box_sizer_vertical.Add(self.list_box, proportion=1, flag=wx.EXPAND)


    def RegisterFrameEvents(self):
        """Register events to the Frame.
        """

        # wx.Window.RegisterHotKey
        # https://wxpython.org/Phoenix/docs/html/wx.Window.html?highlight=registerhotkey#wx.Window.RegisterHotKey
        new_id = wx.NewIdRef(count=1)
        self.RegisterHotKey(new_id, wx.MOD_ALT, wx.WXK_SPACE)
        self.Bind(wx.EVT_HOTKEY, self.OnHotKeyAltSpace, id=new_id)

        # Register accelerator table.
        # NOTE: An accelerator table is a data resource that maps keyboard combinations, such as CTRL+O.
        new_id_for_exit = wx.NewIdRef()
        self.Bind(wx.EVT_MENU, self.OnCtrlQ, new_id_for_exit)
        # wx.AcceleratorTable
        # https://wxpython.org/Phoenix/docs/html/wx.AcceleratorTable.html
        # wx.AcceleratorEntryFlags(Such as wx.ACCEL_CTRL)
        # https://wxpython.org/Phoenix/docs/html/wx.AcceleratorEntryFlags.enumeration.html
        accelerator_table = wx.AcceleratorTable([
            # NOTE: wx.ACCEL_CTRL corresponds to Command key on OS X.
            (wx.ACCEL_CTRL, ord('q'), new_id_for_exit),
        ])
        self.SetAcceleratorTable(accelerator_table)

        # wx.ActivateEvent
        # https://wxpython.org/Phoenix/docs/html/wx.ActivateEvent.html
        # The event, called when the Frame gets either active and inactive.
        self.Bind(wx.EVT_ACTIVATE, self.OnSwitchActiveState)

        # Key input event.
        self.Bind(wx.EVT_CHAR_HOOK, self.OnChar)


    def OnHotKeyAltSpace(self, event):

        util.WxUtil.Show(self)

    def OnCtrlQ(self, command_event):

        # wx.Exit
        # https://wxpython.org/Phoenix/docs/html/wx.functions.html#wx.Exit
        # FIXME: Since document says that this method 'Should only be used in an emergency,' should be replace with Close or Destroy.
        # But they doesn't work. Leave on other issues.
        wx.Exit()
        self.window_is_alive = False

    def OnChar(self, event):

        # wx.KeyCode
        # https://wxpython.org/Phoenix/docs/html/wx.KeyCode.enumeration.html

        keycode = event.GetKeyCode()

        # Esc: Hide the Frame.
        if keycode == wx.WXK_ESCAPE:
            self.Hide()

        # Up: Move ListBox's cursor up.
        elif keycode == wx.WXK_UP:
            _ = self.list_box.GetSelection() - 1
            if _ >= 0:
                self.list_box.SetSelection(_)
                self.list_box.EnsureVisible(_)

        # Down: Move ListBox's cursor down.
        elif keycode == wx.WXK_DOWN:
            _ = self.list_box.GetSelection() + 1
            if _ <= self.list_box.GetCount() - 1:
                self.list_box.SetSelection(_)
                self.list_box.EnsureVisible(_)

        # Return: Do somthing, using selected item.
        # FIXME: wx._core.wxAssertionError occurs when without items on list.
        elif keycode == wx.WXK_RETURN:
            self.OnReturn()

        else:
            # With input keys, focus TextCtrl.
            self.text_ctrl.SetFocus()

            # Without Skip, all keys but above ↑ are ignored.
            # https://wxpython.org/Phoenix/docs/html/wx.Event.html#wx.Event.Skip
            event.Skip(skip=True)

    def OnSwitchActiveState(self, activate_event):

        # Check if the Window exists.
        # NOTE: Even when the Window is closed or destroyed, ActivateEvent occurs.
        # In that case, events below are not required because the Windows no longer exist. More sincerely, an error occurs.
        #     RuntimeError: wrapped C/C++ object of type DespacitoFrame has been deleted
        if not self.window_is_alive:
            return

        # wx.ActivateEvent.GetActive
        # https://wxpython.org/Phoenix/docs/html/wx.ActivateEvent.html#wx.ActivateEvent.GetActive
        # GetActive returns the state(active or not) that the Windows is going to be.
        if activate_event.GetActive():
            self.OnActive()
        else:
            self.OnInactive()


    def OnActive(self):
        """Event for when the Frame is getting active."""

        pass


    def OnInactive(self):
        """Event for when the Frame is getting inactive."""

        self.Hide()


    def OnText(self, event):

        # Get input text.
        input_text = self.text_ctrl.GetValue()

        # Get items to display on the list, based on the keyword.
        items = despacito_searcher.DespacitoSearcher.search(input_text)

        # Display items on the list.
        self.list_box.Clear()
        for item in items:
            # No document is found.
            # Append(value for display, inner value)
            self.list_box.Append(os.path.basename(item), item)

        # Select the top item if items exist.
        if items:
            self.list_box.SetSelection(0)


    def OnReturn(self):
        """On enter(return) event."""

        if not self.list_box:
            return

        # NOTE: How to get content from wx.ListBox
        # ListBox.GetSelection(): index
        # ListBox.GetStringSelection(): Left arg of ListBox.Append()
        # ListBox.GetClientData(ListBox.GetSelection()): Right arg of ListBox.Append()
        abspath = os.path.abspath(
            self.list_box.GetClientData(self.list_box.GetSelection()))
        util.Util.open_file(abspath)
