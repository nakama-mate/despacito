from enum import Enum, auto
import platform
import os
import subprocess


class WxUtil:
    """Util methods related to wxPython lib."""

    @staticmethod
    def Show(window):
        """Show window and focus on it."""

        # Show() only does display the Window.
        window.Show(show=True)

        # The way to focus on the Window differs depends on the platform.
        platform = Util.get_platform()
        if platform == Platform.WINDOWS:
            # wx.Window.Raise
            # https://wxpython.org/Phoenix/docs/html/wx.Window.html#wx.Window.Raise
            window.Raise()
        elif platform == Platform.MAC:
            subprocess.Popen(['osascript', '-e', '''\
                tell application "System Events"
                set procName to name of first process whose unix id is %s
                end tell
                tell application procName to activate
            ''' % os.getpid()])


class Util:
    """Util methods for general purposes."""

    @staticmethod
    def get_platform():
        """Get platform on where the app works, as Enum."""
        pf = platform.system()
        if pf == 'Windows':
            return Platform.WINDOWS
        elif pf == 'Darwin':
            return Platform.MAC
        elif pf == 'Linux':
            return platform.LINUX
        else:
            return platform.LINUX

    @staticmethod
    def open_file(abspath: str) -> None:
        """Open file."""

        # The way to open file differs depends on the platform.
        platform = Util.get_platform()
        if platform == Platform.WINDOWS:
            os.startfile(abspath)
        elif platform == Platform.MAC:
            subprocess.run(['open', abspath], check=True)


class Platform(Enum):
    """Platforms."""

    WINDOWS = auto()
    MAC = auto()
    LINUX = auto()

    def get_name(self):
        return {
            self.WINDOWS: 'Windows',
            self.MAC: 'Mac',
            self.LINUX: 'Linux',
        }[self]

    def get_desc(self):
        return {
            self.WINDOWS: 'Dislike',
            self.MAC: 'Like',
            self.LINUX: 'Like',
        }[self]
