import os, sys

class DespacitoRouter():
	root = os.path.dirname(os.path.realpath(__file__))

	@staticmethod
	def make_path_under_root(file_name: str) -> str:
		"""Make path including file name at root direcotry
		:param file_name
			file name including file extension (ex: xxx.txt)
		"""
		if getattr(sys, 'frozen', False):
			"""Return exe file path when using exe file

			"""
			return os.path.join(
				os.path.dirname(sys.executable),
				*['..', 'src', file_name]
			)
		else:
			"""Return main.py file path when using command
				window to execute file

			"""
			return os.path.join(
				DespacitoRouter.root,
				file_name
			)

	