import despacito_app
import despacito_frame
import despacito_taskbaricon

if __name__ == '__main__':

    app = despacito_app.DespacitoApp()
    frame = despacito_frame.DespacitoFrame()
    despacito_taskbaricon.DespacitoTaskBarIcon(frame)
    app.MainLoop()
