import fnmatch
import os

class DespacitoSearcher:
    """Methods related to searching."""

    @staticmethod
    def search(keyword: str) -> list:
        """This is a method for searching.
        This is called from DespacitoFrame.OnText.
        TODO: need to refactoring for 1) listing except for low similarity
              and 2) serching asynchronously
        """
        res = []
        res_add = res.append
        root_path = '/'
        i = 0
        for root, dirs, files in os.walk(root_path):
            for filename in fnmatch.filter(files, keyword + '*'):
                res_add(os.path.join(root, filename))
                i += 1
                if i > 4:
                    break
            if i > 4:
                break
        return res


    @staticmethod
    def ld_algo(p: str, t: str) -> float:
        """Levenshtein distance algorithm
        Use the method to score target by similarity.
        If high cost, low similarity
        """
        lp, lt = len(p), len(t)
        if lp == 0 or lt == 0: return lt^lp
        dp = [[0 for _ in range(lt+1)] for _ in range(lp+1)]
        for i in range(1, lp+1):
            for j in range(1, lt+1):
                dp[0][j] = j
                dp[i][0] = i
        for i in range(1, lp+1):
            for j in range(1, lt+1):
                cost = 1 if p[i-1] != t[j-1] else 0
                dp[i][j] = min(dp[i-1][j-1]+cost, dp[i-1][j]+1, dp[i][j-1]+1)
        return 1 - dp[lp][lt]/(lp+lt)

