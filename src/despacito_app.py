import wx


# wx.App
# https://wxpython.org/Phoenix/docs/html/wx.App.html
class DespacitoApp(wx.App):

    def __init__(self):

        super(DespacitoApp, self).__init__(
            # Should sys.stdout and sys.stderr be redirected?
            redirect=False,
            # The name of a file to redirect output to, if redirect is True.
            filename=None,
            useBestVisual=True,
            clearSigInt=True,
        )


    # wx.AppConsole.OnInit
    # https://wxpython.org/Phoenix/docs/html/wx.AppConsole.html#wx.AppConsole.OnInit
    def OnInit(self):

        return True
