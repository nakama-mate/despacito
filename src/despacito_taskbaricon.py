import wx
import wx.adv
from despacito_router import DespacitoRouter as DR


# wx.adv.TaskBarIcon
# https://wxpython.org/Phoenix/docs/html/wx.adv.TaskBarIcon.html
class DespacitoTaskBarIcon(wx.adv.TaskBarIcon):

    def __init__(self, frame):

        self.frame = frame
        super(DespacitoTaskBarIcon, self).__init__()

        # wx.adv.TaskBarIcon.SetIcon
        # https://wxpython.org/Phoenix/docs/html/wx.adv.TaskBarIcon.html#wx.adv.TaskBarIcon.SetIcon
        icon = wx.Icon(wx.Bitmap(DR.make_path_under_root('hugging.ico')))
        self.SetIcon(
            icon,
            tooltip='Despacito'
        )

        # On Windows system only.
        # The icon set above doesn't work without Binding some sort of instance method.
        # WHY NOT? We had to bind unnecessary event for that.
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DCLICK, self.OnLeftDclick)


    def OnLeftDclick(self, event):

        pass


    def CreatePopupMenu(self):

        menu = wx.Menu()

        # NOTE: This time, because this MenuItem is on TaskBarIcon the shortcut doesn't work.
        # But we would better note that we can set shortcut by using text, '&[OptionalText]\t[ShortCut]'.
        item = wx.MenuItem(parentMenu=menu, id=wx.ID_EXIT, text='&Quit\tCtrl+Q')
        menu.Bind(wx.EVT_MENU, lambda event: wx.Exit(), id=item.GetId())
        menu.Append(item)
        return menu
